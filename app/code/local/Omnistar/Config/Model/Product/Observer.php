<?php

class Omnistar_Config_Model_Product_Observer extends Mage_Core_Model_App
{

	protected $_storeId;

	public function __construct()
	{
	}


	protected function _getStoreId()
	{
		if ( $this->_storeId === null ) {
			$request = Mage::app()->getRequest();
			$this->_storeId = $request->getParam( 'store' );
		}

		return $this->_storeId;
	}


	public function applyAutoSku($observer)
	{
		$product = $observer->getEvent()->getProduct();

		if ( !$product instanceof Mage_Catalog_Model_Product || !$product->getId() ) {
			throw new Exveption( 'Product not set!' );
		}

		try {
			$productId = $product->getId();
			$product = Mage::getModel( 'catalog/product' )
				->setStoreId( $this->_getStoreId() )
				->load( $productId );

			if ( strlen( $product->getSku() ) > 0 ) {
				return $this;
			}

			$post = $this->getRequest()->getPost();

			$prefix = ( isset( $post[ 'category_main' ] && $post[ 'category_main' ] > 0 ) ) ? '9' : '11';

			$sku = $prefix . str_repeat( '0', 7 - strlen( (string)$productId) ) . $productId;

			$product->setStoreId( $this->_getStoreId() )
				->setSku( $sku )
				->save();
				
		} catch ( Exception $ex ) {
			Mage::logException($ex);
			die;
		}

		return $this;
	}

}